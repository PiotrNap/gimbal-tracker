import React, { useEffect, useState } from "react";
import { graphql } from "gatsby";
import { useStoreState } from "easy-peasy";
import { fromBech32, fromHex, toStr } from "../utils/converter";
import { Spinner, Flex, Heading, Text, Box, Button, FormControl, FormLabel, Input, Select } from "@chakra-ui/react";
import { Formik, useFormik } from 'formik';

import { simpleTx } from "../cardano/simple-tx";

import useWallet from "../hooks/useWallet";
import {
    assetsToValue,
    createTxOutput,
    finalizeTx,
    initializeTx,
    serializeTxUnspentOutput,
    valueToAssets,
} from "../cardano/transaction";


// from connected wallet, get all UTXOs
function getWalletUtxoStrings(wallet) {
  const utxoStrings = wallet.utxos
      .map((utxo) => serializeTxUnspentOutput(utxo).output())
      .map((txOut) => valueToAssets(txOut.amount()))
  return [...new Set(utxoStrings)];
};

// given a hex-encoded Unit in a Value pair, return the policyID
function getPolicyId(unit) {
  let id = ""
  if(unit == "lovelace") {
      return ""
  }
  else {
      id = unit.slice(0,56)
  }
  return id
}

// given a hex-encoded Unit in a Value pair, return the asset name
function getTokenName(unit) {
  let name = ""
  if(unit == "lovelace") {
      return "ada"
  }
  else {
      let temp = fromHex(unit.substring(56))
      name = toStr(temp)
  }
  return name
}

function getQuantity(unit, quantity) {
  if(unit == "lovelace") {
      return quantity/1000000
  }
  return quantity
}

function getWalletAssetValues(wallet) {
  const utxoStrings = getWalletUtxoStrings(wallet);
  const assetValues = utxoStrings.map((utxoString) => utxoString.map((currentValue) => ({
      policy: getPolicyId(currentValue.unit),
      unit: getTokenName(currentValue.unit),
      quantity: getQuantity(currentValue.unit, currentValue.quantity)
  })))
  return [...new Set(assetValues)]
}

// Look for a particular asset in a wallet
function walletHoldsThisAsset(assetList, policyId, name) {
  let quantity = 0
  console.log("hold me", assetList)
  assetList.map((valueList) => {
      valueList.map((asset) => {
        if ((asset.policy === policyId) && (asset.unit === name)){
            console.log("assets", asset.policy, "name", asset.unit)
            quantity = asset.quantity
        }
      })
  })

  return quantity
}

const MetadataTx = ({data}) => {
  const edges = data.allMarkdownRemark.edges

  const connected = useStoreState((state) => state.connection.connected);
  const [walletAddress, setWalletAddress] = useState(null);
  const [walletBalance, setWalletBalance] = useState("");
  const [walletAssets, setWalletAssets] = useState([]);
  const [walletUtxos, setWalletUtxos] = useState([]);
  const { wallet } = useWallet(null);
  const [loading, setLoading] = useState(true);

  const [gimbalTokens, setGimbalTokens] = useState(0);

  // on loading change, if wallet is connected, set wallet address
  useEffect(async () => {
    if (connected && wallet) {
      setWalletBalance(wallet.balance);
      setWalletAddress(wallet.address);
      setWalletAssets(getWalletAssetValues(wallet));
      console.log("what about here", getWalletAssetValues(wallet))
    }
  }, [loading]);

  useEffect(async () => {
    if (connected && wallet) {
      const myUtxos = await wallet.utxos;
      setWalletUtxos(wallet.utxos);
      setLoading(false);
    }
  }, [wallet]);

  useEffect(() => {
    if (connected && wallet){
      console.log("hello gimbals", walletHoldsThisAsset(walletAssets, "2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c30", "gimbal"))
      setGimbalTokens(walletHoldsThisAsset(walletAssets, "2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c30", "gimbal"))
    }
  }, [walletAssets])

  useEffect(() => {
    if (walletUtxos.length == 0) {
      setLoading(true);
    }
    if (walletAddress == null || walletAddress == "") {
      setLoading(true);
    }
    if (loading) {
      setLoading(false);
    }
  }, [walletUtxos, setWalletAddress]);

  const sendMyTransaction = async () => {
    const fromMyWallet = {
      "address": fromBech32(walletAddress),
      "utxosParam": walletUtxos,
      "recAddr": recAddr,
      "memo": metadataMsg,
      "gimbals": gimbalsToSend,
    }
    const txHash = await simpleTx(fromMyWallet)
    setLoading(true)
  }

  const formik = useFormik({
    initialValues: {
      receivingAddress: "",
      metadataMessage: "",
    },
  })

  const [recAddr, setRecAddr] = useState(formik.receivingAddress)
  const [metadataMsg, setMetadataMsg] = useState(formik.metadataMessage)
  const [gimbalsToSend, setGimbalsToSend] = useState(formik.numGimbalTokensToSend)


  useEffect(() => {
    const a = formik.values.receivingAddress;
    setRecAddr(a);
  }, [formik.values.receivingAddress])

  useEffect(() => {
    const a = formik.values.metadataMessage;
    const bountyId = a.substring(0,10)
    setMetadataMsg(bountyId);
  }, [formik.values.metadataMessage])

  useEffect(() => {
    const a = formik.values.numGimbalTokensToSend;
    setGimbalsToSend(a);
  }, [formik.values.numGimbalTokensToSend])

  return (
    <>
      <title>demo v0</title>
      <Flex
        w="100%"
        mx="auto"
        direction="column"
        wrap="wrap"
        bg="gl-yellow"
        p="10"
      >
        <Box w="50%" mx="auto" my='5'>
          <Heading size="4xl" color="gl-blue" fontWeight="medium" py="5">
            Send Gimbals
          </Heading>
          <Text p='1' color="gl-red" fontSize='sm'>Wallet Balance: {walletBalance} Ada</Text>
          <Text p='1' color="gl-red" fontSize='sm'>Gimbal Tokens: {gimbalTokens/1000000}</Text>
          <Text p='1' color="gl-red" fontSize='sm'>Connected Address: {walletAddress}</Text>
          <Text p='2' color="gl-blue">This tool is still a proof-of-concept and not yet ready for public use. Next steps are documented across the site, and (speaking of next steps) will soon be listed as bounties.</Text>
        </Box>
        <Box w="50%" p='5' mx="auto" bg="gl-blue" color='white'>
          <FormControl>
            <FormLabel fontWeight="bold">
              Enter Receive Address (next step: should autopopulate if address is assigned to bounty)
            </FormLabel>
            <Input name="receivingAddress" onChange={formik.handleChange} value={formik.values.receivingAddress} />
            <FormLabel pt='5' fontWeight="bold">
              Attach to Bounty: (next step: completed bounties should not show in this menu)
            </FormLabel>
            <Select name="metadataMessage" onChange={formik.handleChange} value={formik.values.metadataMessage} color='black' placeholder='Select Bounty'>
              {edges.map(edge => {
                const {frontmatter} = edge.node
                return (
                  <option>{frontmatter.slug}: {frontmatter.title}</option>
                )
              })}
            </Select>
            <FormLabel pt='5' fontWeight="bold">
              Number of Gimbals to Send (next step: handle decimals)
            </FormLabel>
            <Input name="numGimbalTokensToSend" onChange={formik.handleChange} value={formik.values.numGimbalTokensToSend} />
          </FormControl>
          <Box mt='5'>
            <Text p='1' fontSize='sm'>Send Gimbals to: {recAddr}</Text>
            <Text p='1' fontSize='sm'>Gimbal Distribution Memo: {metadataMsg}</Text>
            <Text p='1' fontSize='sm'>Number of Gimbal Tokens: {gimbalsToSend}</Text>
            <Button m='3' color='gl-blue' onClick={sendMyTransaction}>Send TX</Button>
          </Box>
        </Box>
      </Flex>
    </>
  );
};

export const query = graphql`
    query GetTitles {
        allMarkdownRemark(
            sort: { order: ASC, fields: [frontmatter___slug]}
        ) {
            edges {
                node {
                    frontmatter {
                        title
                        slug
                    }
                }
            }
        }
    }
`

export default MetadataTx;
