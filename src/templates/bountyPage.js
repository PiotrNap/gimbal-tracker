import React from "react";
import { graphql, Link } from "gatsby";
import { Box, Text, Heading, Button, Center, Flex } from "@chakra-ui/react";

import * as style from "./bountyDetails.module.css"

const Template = ({ data, pageContext }) => {
    const { markdownRemark } = data
    const { next, prev } = pageContext
    const title = markdownRemark.frontmatter.title
    const date = markdownRemark.frontmatter.date
    const completed = markdownRemark.frontmatter.completed
    const tags = markdownRemark.frontmatter.tags
    const scope = markdownRemark.frontmatter.scope
    const ada = markdownRemark.frontmatter.ada
    const gimbals = markdownRemark.frontmatter.gimbals
    const html = markdownRemark.html
    return (
        <Box mx='auto' my='10' p='5' bg="white" w='50%'>
            <Heading>
                {title}
            </Heading>
            <Heading size='sm' py='3'>
                Posted: {date} {completed ? `| Completed: ${completed}` : ""}
            </Heading>
            <Heading size='sm' py='3'>
                gimbals: {gimbals} | ada: {ada}
            </Heading>
            <Flex direction='row'>
                {tags?.map((tag) => <Box bg='orange.400' p='2' mx='2' w="15%" textAlign='center' rounded='md'>{tag}</Box>)}
                <Box bg='purple.700' color='white' fontWeight='bold' p='2' mx='2' w='15%' textAlign='center' rounded='md'>{scope}</Box>
            </Flex>
            <Box bg='orange.100' mt='5' p='5'>
                <div dangerouslySetInnerHTML={{ __html: html }} className={style.mdStyle} />
            </Box>
            <Center pt='10'>
                {prev &&
                    <Link to={`/bounties/${prev.frontmatter.slug}`}>
                        <Button mx='3' border='solid' borderColor='gl-blue'>
                            Previous
                        </Button>
                    </Link>
                }
                {next &&
                    <Link to={`/bounties/${next.frontmatter.slug}`}>
                        <Button mx='3' border='solid' borderColor='gl-blue'>
                            Next
                        </Button>
                    </Link>
                }
                <Link to="/bounties">
                        <Button mx='3' bg='gl-green' border='solid' borderColor='gl-green'>
                            View All
                        </Button>
                </Link>
                <Link to="/metadatatx">
                        <Button mx='3' bg='gl-blue' border='solid' borderColor='gl-blue' color='white'>
                            Send Payment
                        </Button>
                </Link>
            </Center>
            <Box border='1px' mt='5' p='5'>
                <Heading>How it Works</Heading>
                <Text py='3'>
                    At Gimbalabs, we are in the process of experimenting with organizational structure, and we are distributing responsibilities. Our intention is to help people find projects, teams, and learning opportunities that inspire them to action.
                </Text>
                <Text py='3'>
                    We are not yet delivering on these intentions: it's a work in progress. That's one reason why this site exists. It is a direct experiment in how I can share my responsibilities by using my personal allocation of Gimbals. If you see a "bounty" that you'd like to take on, please send me DM on Discord (@jamesdunseith#3315), and I'll help you get started.
                </Text>
                <Text py='3'>
                    The other reason that this site exists is to serve as a starting point. There is a set of functionality that I'd like to add and a series of experiments that I think we can run within the context of this project. One example of better functionality will be to automate the process of committing to a bounty: clearly it's not sustainable for someone to DM me every time they want to take on a task. One example of an experiment will be to test the <a href="https://gitlab.com/gimbalabs/ppbl-course-01/ppbl-course-01/-/tree/master/project-04" className='colorLink'>Escrow contract that we're currently building in Plutus PBL</a>.
                </Text>
                <Text py='3'>
                    If this project inspires you to think of additional next steps or your own experiments, then it's a success. And to all Gimbal Token PBL Groups -- I'll happily accept your Gimbals as payment for helping you set up your own instance of this tool!
                </Text>
            </Box>
        </Box>
    )
}

export const query = graphql`
    query($pathSlug: String!) {
        markdownRemark(frontmatter: { slug: { eq: $pathSlug } }) {
            html
            frontmatter {
                title
                date
                completed
                tags
                scope
                ada
                gimbals
            }
        }
    }
`



export default Template